
//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

const products=[
  {
    "name": "Aegis Boost",
    "description" : "Electric cigarette",
    "price" : 1400,
    "stocks" : 100
  },
  {
    "name": "ROG phone 6 pro",
    "description" : "Best gaming android phone",
    "price" :48000,
    "stocks" : 50
  }
 
]

let http = require ('http');

let port = 8000;

const server = http.createServer(function(request, response) {
  
  if(request.url == '/' && request.method == 'GET') { 

      response.writeHead(200, {'Content-Type': 'text/plain'})
      response.end('Welcome to Booking System!')

  } else if(request.url == '/dashboard' && request.method == 'GET') { 

      response.writeHead(200, {'Content-Type': 'text/plain'})
      response.end(' Welcome to your User\'s Dashboard!');

  } else if(request.url == '/products' && request.method == 'GET') { 
    
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.write('Here’s our products available!');
      response.write(JSON.stringify(products))
      response.end();

  } else if(request.url == '/addProduct' && request.method == 'POST') {
    
    let request_body = ''
    request.on('data', function(data) {
      request_body += data
    })

    request.on('end', function() {
      request_body = JSON.parse(request_body)

      let new_product = {
        "name": request_body.name,
        "description": request_body.description,
        "price": request_body.price,
        "stocks":request_body.stocks
      }

      products.push(new_product)
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.write(JSON.stringify(new_product))
      response.end()
    })

  } else if(request.url == '/updateProduct' && request.method == 'PUT') { 

      response.writeHead(200, {'Content-Type': 'text/plain'})
      response.end('Update a product to our resourses!');

  } else if(request.url == '/archiveProduct' && request.method == 'DELETE') { 

      response.writeHead(200, {'Content-Type': 'text/plain'})
      response.end('Archive a product to our resourses!');
 
  }

});


server.listen(port, ()=>{
  console.log(`Server is running at localhost: ${port}`);
});



