
//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)

*/
// code:
const studentScores = [58, 50, 68];

function classSectioning(arrScores){
	let avgScore = getAvg(arrScores);
	let msg = `Your score is ${avgScore}. You will become proceed to Grade 10`;		
	let output= '';	

		if (avgScore <= 80) {
			output =`${msg} Ruby`

		} else if (avgScore <= 120){
			output =`${msg} Opal`

		} else if (avgScore <= 160){
			output =`${msg} Sappire`

		}  else if (avgScore <= 200){
			output =`${msg} Diamond`
		}

	return output || 'Your average score exceeds 200'
 };

function getAvg(arrScores){

	return arrScores.reduce((total, currentValue) => total + currentValue);
	
	//or
	// let avg = 0;
	// for(let i = 0; i < arrScores.length;i++){
		// avg += arrScores[i]
	// }
	// return avg;
}

console.log(classSectioning(studentScores));
console.log('');


/*
2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/

let string = 'Web Development Tutorial';

function getLongestWord(strings){
	let arrStrings = strings.split(' ') 
	return arrStrings.filter(function(word) {
		return word.length  == Math.max(...arrStrings.map(elem => elem.length))
	});
};
console.log(`The longest word in "${string}" is:`);
console.log(getLongestWord(string) + '')
console.log('');

/*
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'
*/

let char = 'abacddbec';

function firstNonRepeatingChar(str) {
  for (let i = 0; i < str.length; i++) {
    if (str.indexOf(str[i]) == i && str.indexOf(str[i], i + 1) == -1) {
      return str[i];
    }
  }
}

console.log(firstNonRepeatingChar(char));